FoToPdf
=======

Create PDF documents from [XSL-FO](https://fr.wikipedia.org/wiki/XSL-FO) files.

This program relies heavily on [Apache FOP](https://xmlgraphics.apache.org/fop). Its main addition is that the format of inputs (arguments)
and outputs (list of produced files, warnings, errors, etc) are especially designed to be used in combination with other program(s) :

- Input _.fo_ files are typically produced by XSLT transformations (see [Java](https://docs.oracle.com/cd/B19306_01/appdev.102/b14252/adx_j_xslt.htm#i1028867)
and [C#](https://support.microsoft.com/en-us/kb/307322) examples).
- Output PDF files are typically sent to a mass mailing software.

Java 6 or above is required.

## Download

See [releases](https://gitlab.com/ipamo/fotopdf/tags) to download `fotopdf.jar` (which includes the program and all its dependencies).
  
  
## Usage

Convert _hello.fo_ into a PDF file (named _hello.pdf_ by default):

	java -jar fotopdf.jar hello.fo

Convert all _.fo_ files within a directory into PDF files:

	java -jar fotopdf.jar my_directory

Combine all _.fo_ files within a directory into a single PDF file (with a layout):

	java -jar fotopdf.jar --combine --layout=my_layout my_directory

Note: append `-Dfile.encoding=utf-8` after `java` command to use UTF-8 input files.

For further options, see documentation:

- [English](src/main/resources/help-en.txt)
- [French](src/main/resources/help-fr.txt)


## Build

The target binary file (`target/fotopdf.jar`) includes FoToPdf and all its dependencies.

### With Eclipse

- Import the project (_File > Import > General > Existing projects into Workspace_).
- Run configuration _mvn-package_ (_Run > Run Configurations > Maven Build > mvn-package_)

### With Maven (from the command line)

- If necessary:
    - [Download Maven](https://maven.apache.org/download.cgi) and add _bin_ subdirectory to your PATH. Alternative (Debian): `aptitude install maven2`.
    - Define JAVA_HOME environment variable to your [Java Development Kit](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) installation (for exemple `C:\Program Files\Java\jdk1.8.0_77`).
- From the project root directory, run:

	mvn clean compile test assembly:single


## License

FoToPdf is open source software licensed under the LGPLv3 (see [LICENSE.txt](LICENSE.txt)).

[![Lesser General Public License](http://ipamo.net/lgpl.png)](https://www.gnu.org/licenses/lgpl-3.0.en.html)

