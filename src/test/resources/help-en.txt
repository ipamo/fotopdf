SYNOPSIS

	java -Dfile.encoding=utf-8 -jar fotopdf.jar [OPTIONS] ARGUMENTS
        
	Creates PDF documents from XSL-FO files.
    
	The path of generated PDF files is identical to the path of input XSL-FO
	paths (with extension replaced by ".pdf"), except if option --out is used.
        
ARGUMENTS

	List of input XSL-FO files or directories. When directories are provided,
	files are searched for recursively in these directories (all files that
	have ".fo" extension are considered as XSL-FO input files).
	
	If argument "-" (a single hyphen) is provided, files and directories are
	also read on standard input stream.
	
	Optional: paths can be prefixed with a tag, using "TAG:PATH" format (for
	example: 123:file.fo). This tag can be used by program output (useful to
	facilate handle of program output by other programs).
    
OPTIONS

	-b, --base=BASE_DIR: prefixes for relative paths used in input XSL-FO
		files.
	
	-c, --combine: combines all outputs in a single PDF file. The path of the
		generated PDF file is determined either by --out option, or by --index
		option (path is then identical to INDEX_FILE, with extension replaced
		by ".pdf"). If none is provided, the generated file will be
		"output.pdf" in the current working directory.
		Option --combine requires option --layout.
		
	-h, --help: displays program help.
	
	-l, --layout=LAYOUT_FILE: uses LAYOUT_FILE as the XSL-FO input file instead
		as the provided input files. The content of input files is actually
		added to the content of LAYOUT_FILE, at the position marked by token  
		${include_page_sequences}.
		This option requires that input files contain a <fo:page-sequence>
		tag instead of the <fo:root> tag.
		
	-o, --out=PATH: generates output PDF file(s) in PATH, instead of in the
		input directories. PATH may be an existing directory, or a file with
		extension ".pdf" (in this case, if the program produces more than
		one output file, an error will be raised).
			
	-v, --version: displays program version.
	
	-x, --index=INDEX_FILE: writes details of performed operations in
		INDEX_FILE rather than on standard output stream.
	