package net.ipamo.fotopdf;

import java.io.File;

public class TestUtils {
	private final static String NEWLINE = System.getProperty("line.separator");
	public final static File TEST_SAMPLES = new File("test-samples");
	public final static File TEST_IMPORTS = new File("test-samples/imports");
	
	private static void deleteAllPdfs(File dir) {
		File[] files = dir.listFiles();
		for (File file : files) {
			if (file.isFile() && file.getPath().endsWith(".pdf")) {
				if (! file.delete())
					throw new RuntimeException("Cannot delete " + file.getPath());
			} else if (file.isDirectory()) {
				deleteAllPdfs(file);
			}
		}
	}
	
	public static void deleteAllPdfs() {
		deleteAllPdfs(TEST_SAMPLES); 
	}
	
	public static String indexHeader() {
		return "INPUT;STATUS;MESSAGE;OUTPUT;STARTPAGE;PAGES" + NEWLINE;
	}
	
	public static String indexLineSuccess(String tag, File inFile, File outFile, int startPage, int pages) {
		return (tag != null ? tag + Converter.TAG_SEPARATOR : "") + inFile + ";INFO;;" + outFile + ";"+startPage+";"+pages+ NEWLINE;
	}
	
	public static String indexLineSuccess(String tag, File inFile, File outFile, int startPage) {
		return indexLineSuccess(tag, inFile, outFile, startPage, 1);
	}
	
	public static String indexLineSuccess(String tag, File inFile, File outFile) {
		return indexLineSuccess(tag, inFile, outFile, 1, 1);
	}
	
	public static String indexLineSuccess(File inFile, File outFile, int startPage) {
		return indexLineSuccess(null, inFile, outFile, startPage, 1);
	}
	
	public static String indexLineSuccess(File inFile, File outFile) {
		return indexLineSuccess(null, inFile, outFile, 1, 1);
	}
}
