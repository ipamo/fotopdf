package net.ipamo.fotopdf;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.OutputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;

public class NoLayoutTest {
	private OutputStream out;
	
	@Before
	public void setUp() {
		TestUtils.deleteAllPdfs();

		out = new ByteArrayOutputStream();
	}
	   
	@Test
	public void testOne() {
		File base = TestUtils.TEST_IMPORTS;
		File inFile = new File(TestUtils.TEST_SAMPLES, "complete/hello.fo");
		String[] args = new String[] { "--base=" + base.getPath(), inFile.getPath() };
		
		ReturnCode returnCode = Program.main(args, new PrintStream(out));

		File outFile = new File(TestUtils.TEST_SAMPLES, "complete/hello.pdf");
		String expectedOut = TestUtils.indexHeader()
				// File
				+ TestUtils.indexLineSuccess(inFile, outFile);
		
		assertEquals(ReturnCode.OK, returnCode);
		assertEquals(expectedOut, out.toString());
	}
	   
	@Test
	public void testSeveral() {
		File base = TestUtils.TEST_IMPORTS;
		File inFile1 = new File(TestUtils.TEST_SAMPLES, "complete/hello.fo");
		File inFile2 = new File(TestUtils.TEST_SAMPLES, "complete/salut.fo");
		String[] args = new String[] { "--base=" + base.getPath(), inFile1.getPath(), inFile2.getPath() };
		
		ReturnCode returnCode = Program.main(args, new PrintStream(out));

		File outFile1 = new File(TestUtils.TEST_SAMPLES, "complete/hello.pdf");
		File outFile2 = new File(TestUtils.TEST_SAMPLES, "complete/salut.pdf");
		String expectedOut = TestUtils.indexHeader()
				// File1
				+ TestUtils.indexLineSuccess(inFile1, outFile1)
				// File2
				+ TestUtils.indexLineSuccess(inFile2, outFile2);
		
		assertEquals(ReturnCode.OK, returnCode);
		assertEquals(expectedOut, out.toString());
	}
	   
	@Test
	public void testDir() {
		File base = TestUtils.TEST_IMPORTS;
		File inDir = new File(TestUtils.TEST_SAMPLES, "complete");
		String[] args = new String[] { "--base=" + base.getPath(), inDir.getPath() };
		
		ReturnCode returnCode = Program.main(args, new PrintStream(out));

		String outString = out.toString();
		File inFile1 = new File(TestUtils.TEST_SAMPLES, "complete/hello.fo");
		File inFile2 = new File(TestUtils.TEST_SAMPLES, "complete/salut.fo");
		File outFile1 = new File(TestUtils.TEST_SAMPLES, "complete/hello.pdf");
		File outFile2 = new File(TestUtils.TEST_SAMPLES, "complete/salut.pdf");
		
		assertEquals(ReturnCode.OK, returnCode);
		assertThat(outString, startsWith(TestUtils.indexHeader()));
		assertThat(outString, containsString(TestUtils.indexLineSuccess(inFile1, outFile1)));
		assertThat(outString, containsString(TestUtils.indexLineSuccess(inFile2, outFile2)));
	}
	   
	@Test
	public void testCombineDir() {
		File base = TestUtils.TEST_IMPORTS;
		File inDir = new File(TestUtils.TEST_SAMPLES, "complete");
		String[] args = new String[] { "--combine", "--base=" + base.getPath(), inDir.getPath() };
		
		OutputStream err = new ByteArrayOutputStream();
		ReturnCode returnCode = Program.main(args, new PrintStream(out), new PrintStream(err));
		
		assertEquals(ReturnCode.CRITICAL, returnCode);
		assertThat(err.toString(), containsString("[FATAL] L'option --combine ne peut pas être utilisée sans l'option --layout"));
	}
}
