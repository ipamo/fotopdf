package net.ipamo.fotopdf;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;

public class LayoutTest {
	private final static String NEWLINE = System.getProperty("line.separator");
	private OutputStream out;
	
	@Before
	public void setUp() {
		TestUtils.deleteAllPdfs();

		out = new ByteArrayOutputStream();
	}
	
	@Test
	public void testPageSequence() throws Exception {
		File base = TestUtils.TEST_IMPORTS;
		
		File layout = new File(TestUtils.TEST_SAMPLES, "layout.fo");
		File inFile1 = new File(TestUtils.TEST_SAMPLES, "page-1.fo");
		File inFile2 = new File(TestUtils.TEST_SAMPLES, "page-2.fo");
		File inFile3 = new File(TestUtils.TEST_SAMPLES, "page-3.fo");
		String inTag1 = "tag1";
		String inTag2 = "tag2";
		String inTag3 = "tag3";
		File outFile = new File(TestUtils.TEST_SAMPLES, "output.pdf");
		
		String input = inTag1 + Params.TAG_SEPARATOR + inFile1.getPath() + NEWLINE
				+ inTag2 + Params.TAG_SEPARATOR + inFile2.getPath() + NEWLINE
				+ inTag3 + Params.TAG_SEPARATOR + inFile3.getPath() + NEWLINE
				+ NEWLINE;
		
		String[] args = new String[] { "--combine", "--base=" + base.getPath(), "--out=" + outFile.getPath(), "--layout=" + layout.getPath(), "-" };
		InputStream inStream = new ByteArrayInputStream(input.getBytes("UTF-8"));
		
		ReturnCode returnCode = Program.main(args, new PrintStream(out), null, inStream);

		String expectedOut = TestUtils.indexHeader()
				// File1
				+ TestUtils.indexLineSuccess(inTag1, inFile1, outFile, 1)
				// File2
				+ TestUtils.indexLineSuccess(inTag2, inFile2, outFile, 2)
				// File3
				+ TestUtils.indexLineSuccess(inTag3, inFile3, outFile, 3);
		
		assertEquals(ReturnCode.OK, returnCode);
		assertEquals(expectedOut, out.toString());
	}
}
