package net.ipamo.fotopdf;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import org.apache.commons.logging.Log;

public class Params {

	private final static String NEWLINE = System.getProperty("line.separator");
	
	final static char TAG_SEPARATOR = ':';

	
	private final Log log = LogManager.getLogger(Params.class);
	
	private boolean version = false;
	private boolean help = false;
	private File base = null;
	private File out = null;
	private File index = null;
	private File layout = null;
	private boolean combine = false;
	private boolean waitForInput = false;
	private Set<String> inputs = new TreeSet<String>();

	// key: path, value: tag
	private Map<String, String> tags = new HashMap<String, String>();
	
	public boolean hasVersion() {
		return version;
	}
	public boolean hasHelp() {
		return help;
	}
	public File getBase() {
		return base;
	}
	public File getOut() {
		return out;
	}
	public File getIndex() {
		return index;
	}
	public File getLayout() {
		return layout;
	}
	public boolean hasCombine() {
		return combine;
	}
	public boolean hasWaitForInput() {
		return waitForInput;
	}
	public Set<String> getInputs() {
		return inputs;
	}
	public Map<String, String> getTags() {
		return tags;
	}
	
	public Params(String[] params) {

		List<String> argsList = new ArrayList<String>(params.length);

		boolean noMoreOptions = false;

		for (int i = 0; i < params.length; i++) {
			String param = params[i];

			if (param.length() == 0) {
				// Ignoré
			}

			else if (noMoreOptions) {
				argsList.add(param);
			}

			else if (param.length() == 1) {
				// Considère tout paramètre ne comportant qu'un seul caractère
				// (y compris le tiret seul) comme un argument.
				argsList.add(param);
			}

			else if (param.equals("--")) {
				// Le double tiret a pour effet de terminer la lecture des
				// options : tout argument qui suit
				/// est traité comme un argument, même s'il commence par un ou
				// deux tirets.
				noMoreOptions = true;
			}

			else if (param.charAt(0) == '-' && param.length() >= 2 && param.charAt(1) != '-' && !param.contains("=")) {
				// Parse les options courtes sans valeurs, rassemblées derrière
				// un seul tiret
				for (int j = 1; j < param.length(); j++) {
					String name = param.substring(j, j + 1);
					if (!name.equals(" "))
						parseOption(name, null);
				}
			}

			else if (param.charAt(0) == '-' && param.length() >= 2) {
				// Parse toutes les autres options (longues avec ou sans
				// valeurs, courtes avec valeur)
				while (param.startsWith("-"))
					param = param.substring(1);

				if (!param.equals("")) {
					int pos = param.indexOf("=");
					if (pos > 0) {
						String name = param.substring(0, pos).trim();
						String value = param.substring(pos + 1);
						parseOption(name, value);
					} else {
						parseOption(param.trim(), null);
					}
				}
			}

			else {
				argsList.add(param);
			}
		}

		if (version)
			printVersion();

		if (help)
			printHelp();

		// Parse les arguments
		String[] args = argsList.toArray(new String[] {});
		parseArguments(args);
	}

	private void parseArguments(String args[]) {
		for (int i = 0; i < args.length; i++) {
			String arg = args[i];
			
			if (arg == null)
				continue;

			if (arg.equals("-") || arg.equals("--")) {
				waitForInput = true;
				continue;
			}

			if (!arg.trim().isEmpty()) {
				inputs.add(arg.trim());
			}
		}
	}

	/**
	 * Parse une option.
	 * 
	 * @param name
	 *            Nom de l'option.
	 * @param value
	 *            Valeur de l'option, ou {@code null} si l'option n'a pas de
	 *            valeur.
	 */
	private void parseOption(String name, String value) {
		if (name.equalsIgnoreCase("version") || name.equalsIgnoreCase("v")) {
			version = true;
		} else if (name.equalsIgnoreCase("help") || name.equalsIgnoreCase("h")) {
			help = true;
		} else if (name.equalsIgnoreCase("base") || name.equalsIgnoreCase("b")) {
			if (value == null)
				log.warn("Option " + name + " ignorée : aucune valeur n'a été fournie");
			else
				base = new File(value);
		} else if (name.equalsIgnoreCase("out") || name.equalsIgnoreCase("o")) {
			if (value == null)
				log.warn("Option " + name + " ignorée : aucune valeur n'a été fournie");
			else
				out = new File(value);
		} else if (name.equalsIgnoreCase("layout") || name.equalsIgnoreCase("l")) {
			if (value == null)
				log.warn("Option " + name + " ignorée : aucune valeur n'a été fournie");
			else
				layout = new File(value);
		} else if (name.equalsIgnoreCase("index") || name.equalsIgnoreCase("x")) {
			if (value == null)
				log.warn("Option " + name + " ignorée : aucune valeur n'a été fournie");
			else {
				index = new File(value).getAbsoluteFile();
			}
		} else if (name.equalsIgnoreCase("combine") || name.equalsIgnoreCase("c")) {
			combine = true;
		} else {
			log.warn("Option " + name + " inconnue");
		}
	}

	// Disponible uniquement au sein d'un JAR
	private synchronized void printVersion() {
		String packageName = Program.class.getPackage().getName();

		try {
			Enumeration<URL> resources = Program.class.getClassLoader().getResources("META-INF/MANIFEST.MF");
			while (resources.hasMoreElements()) {
				Manifest manifest = new Manifest(resources.nextElement().openStream());
				Attributes attributes = manifest.getMainAttributes();
				String mainClass = attributes.getValue("Main-Class");
				String title = attributes.getValue("Specification-Title");
				String version = attributes.getValue("Specification-Version");
				if (mainClass != null && mainClass.equals(Program.class.getName()) && version != null) {
					if (title == null)
						title = packageName;
					
					// Sur la sortie d'erreur (pour ne pas polluer la sortie standard)
					Program.err.println(title + ": version " + version);
					return;
				}
			}
		} catch (IOException e) {
			log.warn("Erreur lors de la recherche de la version", e);
		}

		log.warn(packageName + ": version indisponible");
	}

	private synchronized void printHelp() {
		String lang = System.getProperty("user.language");
		if (lang == null || !lang.equals("fr"))
			lang = "en";
		
		InputStream is = null;
		BufferedReader reader = null;
		StringBuilder builder = new StringBuilder();
		try {
			is = Program.class.getResourceAsStream("/help-"+lang+".txt");
			reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
			String line;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
				builder.append(NEWLINE);
			}
		} catch (Exception e) {
			log.warn("Erreur lors de la recherche de l'aide", e);
		} finally {
			if (reader != null)
				try {
					reader.close();
				} catch (IOException e) {
				}
		}

		String help = builder.toString().replace("\t", "    ");
		
		// Sur la sortie d'erreur pour ne pas poluer la sortie standard
		Program.err.println(help);
	}
}
