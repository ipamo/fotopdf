package net.ipamo.fotopdf;

public enum ReturnCode {
	OK(0), WARNING(1), CRITICAL(2);

	private final int code;

	private ReturnCode(int code) {
		this.code = code;
	}

	public int code() {
		return code;
	}
}
