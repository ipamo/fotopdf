package net.ipamo.fotopdf;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.logging.Log;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.MimeConstants;
import org.apache.fop.apps.PageSequenceResults;

public class CombineConverter extends Converter {
	private final Log log = LogManager.getLogger(CombineConverter.class);
	
	private final File outFile;
	
	public CombineConverter(IndexProducer outputer, File baseDir, File layoutFile, File outFile) throws Exception {
		super(outputer, baseDir, layoutFile);
		this.outFile = outFile;
	}
	
	@Override
	public void convert() {
		if (reportsToDo.isEmpty())
			log.info("Aucun fichier à convertir");

		long startedNanoTime = System.nanoTime();
		boolean deleteOut = false;
		
		// Crée une chaîne XSL-FO à partir des page sequences
		StringBuilder pageSequences = new StringBuilder();

		ConvertEventListener globalListener = new ConvertEventListener();
		List<Report> pageSequencesReports = new ArrayList<Report>(reportsToDo.size());
		for (Report report : reportsToDo) {
			report.started = true;
			try {
				if (! report.inFile.exists()) {
					report.AddError("Le fichier d'entrée n'existe pas");
		    		indexProducer.add(report);
					continue;
				}
				if (! report.inFile.isFile()) {
					report.AddError("Le chemin d'entrée désigne un répertoire");
		    		indexProducer.add(report);
					continue;
				}
				pageSequences.append(interpreteAsPageSequence(report.inFile));
				pageSequences.append(NEWLINE);
				pageSequencesReports.add(report);
				
				report.listener = globalListener; // Le listener est remplacé par un listener global puisque la transformation sera la même pour tous ces fichiers
				report.outFile = outFile;
			}
			catch(Exception e) {
				report.AddError(e.getMessage());
				indexProducer.add(report);
			}
		}
		
		OutputStream out = null;
		try {
			// Prépare l'entrée
			String foContent = applyLayout(pageSequences);
		    Source src = new StreamSource(new StringReader(foContent));
		    
		    // Prépare la sortie
			out = new BufferedOutputStream(new FileOutputStream(outFile));
		    FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
		    foUserAgent.getEventBroadcaster().addEventListener(globalListener);
		    Fop fop = foUserAgent.newFop(MimeConstants.MIME_PDF, out);
		    Result res = new SAXResult(fop.getDefaultHandler());
			
			// Réalise la transformation
		    transformer.transform(src, res);
			
		    // Vérifie que le fichier PDF existe bien
		    if (! outFile.exists()) {
		    	for(Report report : pageSequencesReports)
		    		report.outFile = null;
		    	globalListener.AddError("Le fichier PDF n'a pas été généré");
		    }
		    
		    else {
			    // Met à jour les informations concernant les pages
			    @SuppressWarnings("unchecked")
				List<PageSequenceResults> psrs = fop.getResults().getPageSequences();
			    if (psrs.size() != pageSequencesReports.size()) {
					globalListener.AddError("Nombre de page-sequences généré ("+psrs.size()+") incohérent avec le nombre source ("+pageSequencesReports.size()+")");
			    	deleteOut = true;
			    }
			    else {
				    int currentPage = 1;
				    for (int i = 0; i < psrs.size(); i ++) {
				    	PageSequenceResults psr = psrs.get(i);
				    	Report report = pageSequencesReports.get(i);
				    	report.startPage = currentPage;
				    	report.pages = psr.getPageCount();
				    	currentPage = currentPage + report.pages;
				    }
			    }
		    }
		} catch (Exception e) {
			globalListener.AddError(e.getMessage());
			deleteOut = true;
		} finally {
		    if (out != null)
				try {
					out.close();
				} catch (IOException e) {
				}
		}
		
		// Supprime le fichier généré si nécessaire (par exemple si une SAXParseExecption s'est produite)
		if (deleteOut) {
			if (outFile.exists()) {
				if (! outFile.delete())
					globalListener.AddError("Impossible de supprimer le PDF généré (qui est probablement erroné)");
			}
	    	for(Report report : pageSequencesReports)
	    		report.outFile = null;
		}
		
		// Affichage
		int nbFichiersNonOK = reportsToDo.size() - pageSequencesReports.size();
    	for(Report report : pageSequencesReports) {
    		indexProducer.add(report);
			if (report.getStatus() != IndexLevel.INFO)
				nbFichiersNonOK++;    		
    	}
		
		logStats(startedNanoTime, reportsToDo.size(), nbFichiersNonOK);
		
		reportsToDo.clear();
	}
}
