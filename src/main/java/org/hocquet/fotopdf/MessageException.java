package net.ipamo.fotopdf;

/**
 * For these exceptions, only the message (not the stack trace)
 * will be written on Program err (if the exception is not caught).
 */
public class MessageException extends Exception {
	private static final long serialVersionUID = 1L;

	public MessageException(String message) {
		super(message);
	}
}
