package net.ipamo.fotopdf;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;

public class LogManager {
	private static HashMap<String, Log> loggers = new HashMap<String, Log>();
		
	private static Map<LogLevel, Integer> count = new HashMap<LogLevel, Integer>(6);
	
	public static int getCount(LogLevel level) {
		if (! count.containsKey(level))
			return 0;
		else
			return count.get(level);
	}

	public static void addToCount(LogLevel level, int value) {
		if (! count.containsKey(level))
			count.put(level, value);
		else
			count.put(level, count.get(level) + value);
	}

	public static void addToCount(LogLevel level) {
		addToCount(level, 1);
	}
	
	public static int getTraceCount() { return getCount(LogLevel.TRACE); }
	public static int getDebugCount() { return getCount(LogLevel.DEBUG); }
	public static int getInfoCount() { return getCount(LogLevel.INFO); }
	public static int getWarnCount() { return getCount(LogLevel.WARN); }
	public static int getErrorCount() { return getCount(LogLevel.ERROR); }
	public static int getFatalCount() { return getCount(LogLevel.FATAL); }

	public static void addToTraceCount(int value) { addToCount(LogLevel.TRACE, value); }
	public static void addToDebugCount(int value) { addToCount(LogLevel.DEBUG, value); }
	public static void addToInfoCount(int value) { addToCount(LogLevel.INFO, value); }
	public static void addToWarnCount(int value) { addToCount(LogLevel.WARN, value); }
	public static void addToErrorCount(int value) { addToCount(LogLevel.ERROR, value); }
	public static void addToFatalCount(int value) { addToCount(LogLevel.FATAL, value); }

	public static void addToTraceCount() { addToCount(LogLevel.TRACE, 1); }
	public static void addToDebugCount() { addToCount(LogLevel.DEBUG, 1); }
	public static void addToInfoCount() { addToCount(LogLevel.INFO, 1); }
	public static void addToWarnCount() { addToCount(LogLevel.WARN, 1); }
	public static void addToErrorCount() { addToCount(LogLevel.ERROR, 1); }
	public static void addToFatalCount() { addToCount(LogLevel.FATAL, 1); }

	
	public static Log getLogger(String name) {
		if (writer == null)
			throw new RuntimeException("LogManager n'a pas été configuré");
		
		if (! loggers.containsKey(name)) {
			Log logger = new Logger(writer, name);
			loggers.put(name, logger);
			return logger;	
		}
		else
			return loggers.get(name);	
	}
	
	public static Log getLogger(Class<?> clazz) {
		return getLogger(clazz.getName());		
	}
	
	private static PrintStream writer;
	
	public static void configure(PrintStream writer, boolean resetCount) {
		// Reset
		loggers.clear();		
		if (resetCount)
			count.clear();
		
		// Configure
		System.setProperty("org.apache.commons.logging.Log", Logger.class.getName());
		LogManager.writer = writer;
	}
	
	public static void configure(PrintStream writer) {
		configure(writer, true);
	}
	

	/**
	 * Récupère le type et le message d'un Throwable,
	 * sauf si celui ci est un MessageException.
	 */
	static String throwableMessage(Throwable t) {
		if (t.getClass() == MessageException.class)
			return t.getMessage() != null ? t.getMessage() : "";
		else
			return t.getClass().getName() + (t.getMessage() != null ? ": " + t.getMessage() : "");
	}
}
