package net.ipamo.fotopdf;

public enum LogLevel {
	TRACE,
	DEBUG,
	INFO,
	WARN,
	ERROR,
	FATAL;
}
