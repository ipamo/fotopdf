package net.ipamo.fotopdf;

public enum IndexLevel {
	INFO(1),
	WARN(2),
	ERROR(3);
	
	private final int severity;
	private IndexLevel(int severity) {
		this.severity = severity;
	}
	public int severity() {
		return severity;
	}
}
