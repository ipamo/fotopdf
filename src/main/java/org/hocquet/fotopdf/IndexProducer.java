package net.ipamo.fotopdf;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class IndexProducer {
	
	public final static String escape(String str) {
		if (str == null)
			return null;
		
		return str.replace("\"", "'")
		.replace("\r\n", " | ")
		.replace("\n", " | ")
		.replace("\r", " | ")
		.replace(";", ",")
		.replace("#", "//");
	}

	private final static String NEWLINE = System.getProperty("line.separator");
	
	private final File indexFile;
	private boolean titleDisplayed = false;

	public IndexProducer(File indexFile) {
		this.indexFile = indexFile;
	}

	public IndexProducer() {
		this.indexFile = null;
	}
	
	private void println(String str) {
		if (indexFile != null) {
			Writer out = null;
			try {
				out = new BufferedWriter(new FileWriter(indexFile, true));
				out.append(str);
				out.append(NEWLINE);
			}
			catch (IOException e) {
				throw new RuntimeException(e);
			}
			finally {
				if (out != null)
					try {
						out.close();
					} catch (IOException e) {
					}
			}
		}
		else {
			Program.out.println(str);
		}
	}
		
	public synchronized void add(Report report) {		
		if (! titleDisplayed) {
			println("INPUT;STATUS;MESSAGE;OUTPUT;STARTPAGE;PAGES");
			titleDisplayed = true;
		}

		StringBuilder builder = new StringBuilder();
		
		String inFileWithTag = escape((report.tag != null ? report.tag + ":" : "") + report.inFile.getPath());
		String message = escape(report.getMessage());
		
		// INPUT
		builder.append(inFileWithTag);
		builder.append(";");
		
		// STATUS
		builder.append(report.getStatus().toString());
		builder.append(";");
		
		// MESSAGE
		if (message != null)
			builder.append(message);
		builder.append(";");
		
		// OUTPUT
		if (report.outFile != null)
			builder.append(report.outFile.getPath());
		builder.append(";");
		
		// STARTPAGE
		if (report.startPage != null)
			builder.append(report.startPage);
		builder.append(";");
		
		// PAGES
		if (report.pages != null)
			builder.append(report.pages);

		println(builder.toString());
	}
}
