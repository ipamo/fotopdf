package net.ipamo.fotopdf;

import java.util.regex.Pattern;

import org.apache.fop.events.Event;
import org.apache.fop.events.EventFormatter;
import org.apache.fop.events.EventListener;
import org.apache.fop.events.model.EventSeverity;

public class ConvertEventListener implements EventListener {

	private static Pattern PATTERN_FONT_SUBSTITUTION = Pattern.compile("Font .+ not found.+Substituting with .+");
	
	private StringBuilder messageBuilder;
	private IndexLevel maxLevel;
	
	public IndexLevel getMaxLevel() {
		return maxLevel;
	}
	
	@Override
	public String toString() {
		if (messageBuilder == null)
			return "";
		else
			return messageBuilder.toString();
	}
		
    public void processEvent(Event event) {
        String message = EventFormatter.format(event);
        EventSeverity severity = event.getSeverity();
		
        // Filtre les messages qui ne nous intéressent pas
        if (severity == EventSeverity.INFO)
        	return;
        else if (severity == EventSeverity.WARN && message != null && PATTERN_FONT_SUBSTITUTION.matcher(message).matches())
			return;
		
		// Enregistre le message
        if (severity == EventSeverity.INFO) {
        	Add(IndexLevel.INFO, message);
        }
        else if (severity == EventSeverity.WARN) {
        	Add(IndexLevel.WARN, message);
        }
        else if (severity == EventSeverity.ERROR || severity == EventSeverity.FATAL) {
        	Add(IndexLevel.ERROR, message);
        }
        else {
            assert false;
        }
    }
    
    public void Add(IndexLevel level, String message) {
    	// Met à jour le compteur
        if (level == IndexLevel.INFO) {
        	LogManager.addToInfoCount();
        }
        else if (level == IndexLevel.WARN) {
        	LogManager.addToWarnCount();
        }
        else if (level == IndexLevel.ERROR) {
        	LogManager.addToErrorCount();
        }
        else {
            assert false;
        }
        
        // Met à jour le niveau maximum rencontré
        if (maxLevel == null || maxLevel.severity() < level.severity())
        	maxLevel = level;
        
		// Enregistre le message
		if (messageBuilder == null)
			messageBuilder = new StringBuilder();
		else
			messageBuilder.append(" - ");
		messageBuilder.append(level.toString() + ": " + message);
    }
	
	public void AddInfo(String message) {
		Add(IndexLevel.INFO, message);
	}
	
	public void AddWarn(String message) {
		Add(IndexLevel.WARN, message);
	}
	
	public void AddError(String message) {
		Add(IndexLevel.ERROR, message);
	}
}
