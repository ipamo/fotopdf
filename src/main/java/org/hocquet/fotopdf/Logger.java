package net.ipamo.fotopdf;

import java.io.PrintStream;

import org.apache.commons.logging.Log;

public class Logger implements Log {
	private final PrintStream writer;
	@SuppressWarnings("unused")
	private final String name;
	private final boolean disableCompletely;
	
	public Logger(PrintStream writer, String name) {
		this.writer = writer;
		this.name = name;
		if (name.equals("org.apache.fop.apps.FOUserAgent")) //On utilise ConverterEventListener pour récupérer les informations.
			disableCompletely = true;
		else
			disableCompletely = false;
	}
	
	// Should be used ONLY for external librairies that rely on Apache Commons Logging
	// (required when setting system property: org.apache.commons.logging.Log)
	public Logger(String name) {
		this(Program.err, name);
	}
	
	private synchronized void Log(LogLevel level, Object message, Throwable t) {
		// Met à jour le compteur
		LogManager.addToCount(level);
		
		// Prépare le message
		String render;
		if (message != null && t != null) {
			render = message.toString() + " | " + LogManager.throwableMessage(t);
		}
		else if (message == null && t != null) {
			render = t.getClass().getName() + LogManager.throwableMessage(t);			
		}
		else if (message != null && t == null) {
			if (Throwable.class.isAssignableFrom(message.getClass())) {
				t = (Throwable) message; 
				render = LogManager.throwableMessage(t);
			}
			else {
				render = message.toString();
			}
		}
		else {
			render = "null";
		}
		
		// Affiche le message (systématiquement sur la sortie d'erreur afin de ne pas polluer la sortie standard)
		writer.println("["+level.toString() + "] " + render);
		if (t != null && t.getClass() != MessageException.class)
			t.printStackTrace(writer);
	}

	@Override
	public boolean isTraceEnabled() {
		if (disableCompletely)
			return false;
		return false;
	}
	
	@Override
	public boolean isDebugEnabled() {
		if (disableCompletely)
			return false;
		return false;
	}

	@Override
	public boolean isInfoEnabled() {
		if (disableCompletely)
			return false;
		return true;
	}

	@Override
	public boolean isWarnEnabled() {
		if (disableCompletely)
			return false;
		return true;
	}
	
	@Override
	public boolean isErrorEnabled() {
		if (disableCompletely)
			return false;
		return true;
	}

	@Override
	public boolean isFatalEnabled() {
		if (disableCompletely)
			return false;
		return true;
	}
	
	@Override
	public void trace(Object message) {
		if (isTraceEnabled())
			Log(LogLevel.TRACE, message, null);
	}

	@Override
	public void trace(Object message, Throwable t) {
		if (isTraceEnabled())
			Log(LogLevel.TRACE, message, t);		
	}
	
	@Override
	public void debug(Object message) {
		if (isDebugEnabled())
			Log(LogLevel.DEBUG, message, null);
	}

	@Override
	public void debug(Object message, Throwable t) {
		if (isDebugEnabled())
			Log(LogLevel.DEBUG, message, t);		
	}
	
	@Override
	public void info(Object message) {
		if (isInfoEnabled())
			Log(LogLevel.INFO, message, null);
	}

	@Override
	public void info(Object message, Throwable t) {
		if (isInfoEnabled())
			Log(LogLevel.INFO, message, t);		
	}
	
	@Override
	public void warn(Object message) {
		if (isWarnEnabled())
			Log(LogLevel.WARN, message, null);
	}

	@Override
	public void warn(Object message, Throwable t) {
		if (isWarnEnabled())
			Log(LogLevel.WARN, message, t);		
	}
	
	@Override
	public void error(Object message) {
		if (isErrorEnabled())
			Log(LogLevel.ERROR, message, null);
	}

	@Override
	public void error(Object message, Throwable t) {
		if (isErrorEnabled())
			Log(LogLevel.ERROR, message, t);		
	}
	
	@Override
	public void fatal(Object message) {
		if (isFatalEnabled())
			Log(LogLevel.FATAL, message, null);
	}

	@Override
	public void fatal(Object message, Throwable t) {
		if (isFatalEnabled())
			Log(LogLevel.FATAL, message, t);		
	}
}
