package net.ipamo.fotopdf;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.logging.Log;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;

public class Converter { 
	protected final static String NEWLINE = System.getProperty("line.separator");
	protected final static DecimalFormat DURATION_FORMAT;
	
	static {
		DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
		symbols.setDecimalSeparator('.');
		symbols.setGroupingSeparator(' '); 
		DURATION_FORMAT = new DecimalFormat("0.000", symbols);
		DURATION_FORMAT.setGroupingUsed(false);
	}
	
	private final static String LAYOUT_REPLACEMENT_TOKEN = "${include_page_sequences}";
	protected final static String PAGE_SEQUENCE_OPENING = "<fo:page-sequence";


	private final Log log = LogManager.getLogger(Converter.class);
	
	protected final File outDir;
	protected final IndexProducer indexProducer;
	protected final String layout;
	protected final FopFactory fopFactory;
	protected final Transformer transformer;
	
	public Converter(IndexProducer indexProducer, File baseDir, File layoutFile, File outDir) throws Exception {
		this.outDir = outDir;
		this.indexProducer = indexProducer;
		
		// Interprète le layout s'il a été fourni.
		if (layoutFile != null) {
		    BufferedReader reader = null;
		    StringBuilder builder = new StringBuilder();
		    try {
		    	reader = new BufferedReader(new FileReader(layoutFile));
		    	String line;
		        while((line = reader.readLine()) != null) {
		        	builder.append(line);
		        	builder.append(NEWLINE);
		        }
		    } finally {
		    	if (reader != null)
		    		reader.close();
		    }
		    
		    while (builder.length() > 0 && builder.charAt(0) != '<')
		    	builder.deleteCharAt(0);
		    		    
		    String _layout = builder.toString().trim();
		    
		    if (! _layout.contains(LAYOUT_REPLACEMENT_TOKEN))
		    	throw new MessageException("Le layout fourni ne contient pas le token " + LAYOUT_REPLACEMENT_TOKEN);
		    
		    layout = _layout;
		}
		else
			layout = null;
		
		
		// Utilisation de user.dir si bsaeDir non fourni
		// (user.dir: "The current working directory when the properties were initialized")
		if (baseDir == null)
			baseDir = new File(System.getProperty("user.dir"));
		
		// Vérifications sur le baseDir		
		if (! baseDir.exists())
			throw new MessageException("Le répertoire de base n'existe pas : " + baseDir);
		if (! baseDir.isDirectory())
			throw new MessageException("Le répertoire de base est en fait un fichier : " + baseDir);
		
		// Instanciations pour FOP	
		fopFactory = FopFactory.newInstance(baseDir.toURI());
	    TransformerFactory factory = TransformerFactory.newInstance();
	    transformer = factory.newTransformer(); // identity transformer
	}

	public Converter(IndexProducer indexProducer, File baseDir, File layoutFile) throws Exception {
		this(indexProducer, baseDir, layoutFile, null);
	}
	
	protected StringBuilder interpreteAsPageSequence(File inFile) throws Exception {
		StringBuilder builder = new StringBuilder();
		
		// Récupère le contenu du fichier
	    BufferedReader reader = null;
	    try {
	    	reader = new BufferedReader(new FileReader(inFile));
	    	String line;
	        while((line = reader.readLine()) != null) {
	        	builder.append(line);
	        	builder.append(NEWLINE);
	        }
	    } finally {
	    	if (reader != null)
	    		reader.close();
	    }
	    
	    while (builder.length() > 0 && builder.charAt(0) != '<')
	    	builder.deleteCharAt(0);
	    
	    // Vérifie qu'il y a une seule balise d'ouverture fo:page-sequence, et qu'elle est en première position
	    int pos = builder.indexOf(PAGE_SEQUENCE_OPENING);
	    if (pos != 0)
	    	throw new MessageException("Le fichier d'entrée doit commencer par la balise fo:page-sequence quand l'option --layout est fournie");
	    
	    pos = builder.indexOf(PAGE_SEQUENCE_OPENING, PAGE_SEQUENCE_OPENING.length());
	    if (pos >= 0)
	    	throw new MessageException("Le fichier d'entrée ne peut contenir qu'une seule balise d'ouverture fo:page-sequence");
	    
	    return builder;
	}
	
	protected String applyLayout(StringBuilder template) {
		return layout.replace(LAYOUT_REPLACEMENT_TOKEN, template);
	}
	
	private Source interpreteAsFoSource(File inFile) throws Exception {
		if (layout != null) {
		    // Applique le layout
		    String foContent = applyLayout(interpreteAsPageSequence(inFile));
		    return new StreamSource(new StringReader(foContent));
		}
		else {
			return new StreamSource(inFile);
		}
	}
	
	protected List<Report> reportsToDo = new ArrayList<Report>();
	
	static final char TAG_SEPARATOR = ':';

	private Map<String, File> tags = new HashMap<String, File>();
	
	/**
	 * Ajoute un document source à convertir.
	 * @param foFile
	 */
	protected Report add(String input) {
		Report report = new Report();
		
		int pos = input.indexOf(TAG_SEPARATOR);
		if (pos >= 0) {
			report.tag = input.substring(0, pos).trim();
			if (tags.containsValue(report.tag)) {
				log.warn("Entrée " + input + " ignorée, le tag \"" + report.tag
						+ "\" est déjà utilisé pour un autre fichier ("+tags.get(report.tag)+")");
				return null;
			} else {
				String name = input.substring(pos + 1).trim();
				
				int endDirPos = input.replace('\\', '/').lastIndexOf('/', pos);
				if (endDirPos > 0)
					report.inFile = new File(input.substring(0, endDirPos), name);
				else
					report.inFile = new File(name);
				 
				tags.put(report.tag, report.inFile);
			}
		}
		else {
			report.inFile = new File(input);
		}
		
		reportsToDo.add(report);
		return report;
	}
	
	private void doConvert(Report report) {
		if (! report.inFile.exists()) {
			report.AddError("Le fichier d'entrée n'existe pas");
			return;
		}
		if (! report.inFile.isFile()) {
			report.AddError("Le chemin d'entrée désigne un répertoire");
			return;
		}
		
		boolean deleteOut = false;
				
		// Détermine le nom du fichier PDF de sortie
		String name = report.inFile.getName();
		int pos = name.lastIndexOf(".");
		if (pos >= 0)
			name = name.substring(0, pos) + ".pdf";
		else
			name = name + ".pdf";
		
		File outFile;
		if (outDir != null && outDir.getName().endsWith(".pdf"))
			outFile = outDir;
		else if (outDir != null)
			outFile = new File(outDir, name);
		else if (report.inFile.getParentFile() != null)
			outFile = new File(report.inFile.getParentFile(), name);
		else
			outFile = new File(name);

		if (outFile.exists()) {
			report.AddError("Le fichier de sortie existe déjà");
			return;	
		}
		report.outFile = outFile;
		
		OutputStream out = null;

		try {
			// Prépare l'entrée
		    Source src = interpreteAsFoSource(report.inFile);
		    
		    // Prépare la sortie
			out = new BufferedOutputStream(new FileOutputStream(report.outFile));
		    FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
		    foUserAgent.getEventBroadcaster().addEventListener(report.listener);
		    Fop fop = foUserAgent.newFop(MimeConstants.MIME_PDF, out);
		    Result res = new SAXResult(fop.getDefaultHandler());
		    		    
			// Réalise la transformation
		    transformer.transform(src, res);
		    
		    // Vérifie qu'un fichier PDF existe bien
		    if (! report.outFile.exists()) {
		    	report.outFile = null;
		    	report.AddError("Le fichier PDF n'a pas été généré");
				return;
		    }
		    
		    // Affiche les informations de résultat
		    report.startPage = 1;
		    report.pages = fop.getResults().getPageCount();
		} catch (Exception e) {
			report.AddError(e.getMessage());
			deleteOut = true;
		} finally {
		    if (out != null)
				try {
					out.close();
				} catch (IOException e) {
				}
		}
		
		// Supprime le fichier généré si nécessaire (par exemple si une SAXParseExecption s'est produite)
		if (deleteOut) {
			if (report.outFile.exists()) {
				if (! report.outFile.delete())
					report.AddError("Impossible de supprimer le fichier PDF généré (qui est probablement erroné)");
			}
			report.outFile = null;
		}
	}
	
	protected void logStats(long startedNanoTime, int nbFichiers, int nbFichiersNonOK) {

		double duration = (System.nanoTime() - startedNanoTime) / 1E9D;
		double durationParFichier = duration / (double)nbFichiers; 
		
		if (nbFichiers > 0) {
			String nbFichiers_S = nbFichiers > 1 ? "s" : "";
			String nbFichiersNonOK_NT = nbFichiersNonOK > 1 ? "nt" : "";
			
			String message = nbFichiers+" fichier"+nbFichiers_S+" traité"+nbFichiers_S;
			if (nbFichiersNonOK > 0)
				message += " (dont "+nbFichiersNonOK+" comporte"+nbFichiersNonOK_NT+" au moins une alerte)";
			message += " en "+DURATION_FORMAT.format(duration)+" s";
			if (nbFichiers > 1)
				message += " (soit "+DURATION_FORMAT.format(durationParFichier)+" s par fichier)";		
			log.info(message);
		}
	}
	
	/**
	 * Réalise la ou les conversions demandées et pas encore effectuées.
	 */
	public void convert() {
		if (reportsToDo.isEmpty())
			log.info("Aucun fichier à convertir");
		
		long startedNanoTime = System.nanoTime();
		
		int nbFichiersNonOK = 0;
		for (Report report : reportsToDo) {
			report.started = true;
			doConvert(report);
			indexProducer.add(report);
			if (report.getStatus() != IndexLevel.INFO)
				nbFichiersNonOK++;
		}
		
		logStats(startedNanoTime, reportsToDo.size(), nbFichiersNonOK);
		
		reportsToDo.clear();
	}
}
