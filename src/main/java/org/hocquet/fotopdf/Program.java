package net.ipamo.fotopdf;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;

import org.apache.commons.logging.Log;

/**
 * Cf. src/main/resources/help
 */
public class Program {
	static PrintStream out;
	static PrintStream err;

	private static void addFile(Converter converter, String path) throws IOException {
		converter.add(path);
	}

	private static void addDir(Converter converter, File dir) throws IOException {
		File[] files = dir.listFiles();
		for (File file : files) {
			if (file.isFile()) {
				String name = file.getName();
				int pos = name.lastIndexOf(".");
				if (pos >= 0) {
					String ext = name.substring(pos);
					if (ext.equals(".fo")) {
						String pdfName = name.substring(0, pos) + ".pdf";
						File pdfFile = new File(file.getParent(), pdfName);
						if (!pdfFile.exists()) {
							// Determine tag
							addFile(converter, file.getPath());
						}
					}
				}
			} else if (file.isDirectory()) {
				addDir(converter, file);
			}
		}
	}

	public static ReturnCode main(String[] args, PrintStream _out, PrintStream _err, InputStream _in) {
		Program.out = _out != null ? _out : System.out;
		Program.err = _err != null ? _err : System.err;
		InputStream in = _in != null ? _in : System.in;
		Log log = null;
		
		try {
			// Configure le logging
			LogManager.configure(err);
			log = LogManager.getLogger(Converter.class);
			
			Params params = new Params(args);

			if (params.getInputs().isEmpty() && ! params.hasWaitForInput()) {
				if (params.hasVersion() || params.hasHelp()) {
					return ReturnCode.OK;
				} else {
					throw new MessageException("Aucun fichier ou répertoire fourni en entrée");
				}
			}

			// Vérifie la cohérence des arguments
			if (params.hasCombine() && params.getLayout() == null)
				throw new MessageException("L'option --combine ne peut pas être utilisée sans l'option --layout");

			boolean givenOutDir = false;
			boolean givenOutFile = false;
			if (params.getOut() != null) {
				if (params.getOut().exists()) {
					if (params.getOut().isDirectory())
						givenOutDir = true;
					else
						throw new MessageException("L'option 'out' ne doit pas désigner un fichier existant");
				} else if (params.getOut().getPath().endsWith(".pdf")) {
					File dir = params.getOut().getParentFile();
					if (dir != null && !dir.exists())
						if (!dir.mkdirs())
							throw new MessageException("Impossible de créer le répertoire de sortie : " + dir);
					givenOutFile = true;
				}
			}

			// Prépare l'écriture de l'index (dans un fichier, ou sur la sortie
			// standard)
			IndexProducer indexProducer;
			if (params.getIndex() != null) {
				if (params.getIndex().getPath().trim().endsWith(".pdf")) {
					throw new MessageException("Le chemin du fichier index ne peut pas se terminer par '.pdf'");
				}

				if (params.getIndex().exists()) {
					throw new MessageException("Le fichier index existe déjà : " + params.getIndex());
				}

				File parentDir = params.getIndex().getParentFile();
				if (!parentDir.exists()) {
					if (!parentDir.mkdirs()) {
						throw new MessageException("Impossible de créer le répertoire du fichier index : " + parentDir);
					}
				}

				indexProducer = new IndexProducer(params.getIndex());
			} else {
				indexProducer = new IndexProducer();
			}

			// Crée le convertisseur
			Converter converter;
			if (params.hasCombine()) {
				//

				// Détermine le fichier PDF de sortie
				File outFile;
				if (givenOutFile) {
					outFile = params.getOut();
				} else if (params.getIndex() != null) {
					// Nom du fichier
					String name = params.getIndex().getName();
					int pos = name.lastIndexOf(".");
					if (pos >= 0)
						name = name.substring(0, pos) + ".pdf";
					else
						name = name + ".pdf";

					if (givenOutDir) {
						outFile = new File(params.getOut(), name);
					} else {
						outFile = new File(params.getIndex().getParentFile(), name);
					}
				} else if (givenOutDir) {
					outFile = new File(params.getOut(), "output.pdf");
				} else {
					outFile = new File("output.pdf");
				}

				if (outFile.exists())
					throw new MessageException("Le fichier de sortie existe déjà : " + outFile);

				if (params.getOut() == null)
					log.info("Fichier de sortie : " + outFile);

				File parentDir = outFile.getParentFile();
				if (parentDir != null && !parentDir.exists()) {
					if (!parentDir.mkdirs()) {
						throw new MessageException("Impossible de créer le répertoire du fichier de sortie : " + parentDir);
					}
				}
				converter = new CombineConverter(indexProducer, params.getBase(), params.getLayout(), outFile);
			} else {
				converter = new Converter(indexProducer, params.getBase(), params.getLayout(), givenOutDir || givenOutFile ? params.getOut() : null);	
			}

			// Lit la sortie standard
			if (params.hasWaitForInput()) {
				log.info("Saisissez des fichiers ou répertoires (terminez avec une ligne vide)");

				BufferedReader br = null;
				try {
					br = new BufferedReader(new InputStreamReader(in));
					boolean stop = false;
					do {
						String line = br.readLine();
						if (line == null || line.trim().equals(""))
							stop = true;
						else {
							File file = new File(line.trim());
							if (file.isDirectory())
								addDir(converter, file);
							else
								addFile(converter, line.trim());
						}
					} while (!stop);
				} finally {
					if (br != null)
						br.close();
				}
			}

			// Ajoute les fichiers et répertoires au convertisseur
			for (String input : params.getInputs()) {
				File file = new File(input.trim());
				if (file.isDirectory())
					addDir(converter, file);
				else
					addFile(converter, input.trim());
			}

			// Réalise l'opération de conversion
			converter.convert();

		} catch (Exception e) {
			String message;
			Throwable t; 
			if (e.getClass() == MessageException.class) {
				message = LogManager.throwableMessage(e);
				t = null;
			}
			else {
				message = LogManager.throwableMessage(e);
				t = e;
			}
			
			if (log != null)
				log.fatal(message, t);
			else {
				Program.err.println("[FATAL] " + e.toString());
				if (t != null)
					e.printStackTrace(Program.err);
			}			
		}

		// Code retour
		int errors = LogManager.getErrorCount() + LogManager.getFatalCount();
		int warnings = LogManager.getWarnCount();

		if (errors > 0)
			return ReturnCode.CRITICAL;
		else if (warnings > 0)
			return ReturnCode.WARNING;
		else
			return ReturnCode.OK;
	}


	public static ReturnCode main(String[] args, PrintStream out, PrintStream err) {
		return main(args, out, err, null);
	}

	public static ReturnCode main(String[] args, PrintStream out) {
		return main(args, out, null, null);
	}

	public static void main(String[] args) {
		int returnCode = main(args, null, null, null).code();
		System.exit(returnCode);
	}
}
