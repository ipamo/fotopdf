package net.ipamo.fotopdf;

import java.io.File;

public class Report {			
	public File inFile;
	public String tag;
	public File outFile;
	public Integer startPage;
	public Integer pages;
	public boolean started = false;
	public ConvertEventListener listener = new ConvertEventListener();
	
	public IndexLevel getStatus() {
		if (listener.getMaxLevel() == null && started)
			return IndexLevel.INFO;
		else
			return listener.getMaxLevel();
	}

	public String getMessage() {
		return listener.toString();
	}
	
	public void AddInfo(String message) {
		listener.Add(IndexLevel.INFO, message);
	}
	
	public void AddWarn(String message) {
		listener.Add(IndexLevel.WARN, message);
	}
	
	public void AddError(String message) {
		listener.Add(IndexLevel.ERROR, message);
	}
}
